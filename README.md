#Learn Python

This is a public repository for those wanting to learn Python.

#Course Outline

Content not yet completed is marked with a X-

1. [Basics](basics/basics.md)
	1. [Printing](basics/printing/printing.md)
	1. [Values](basics/values/values.md)
	1. [Variables](basics/vars/vars.md)
	1. [Operators](basics/operators/operators.md)
	1. [If Else Statements](basics/if-else/if-else.md)
	1. [X-For Loops](basics/for/for.md)
	1. [X-Challenge 1: Count to 20 with Even and Odd](challenges/basics/20-even-odd/20-even-odd.md)

#Getting Setup with Python

Installing Python isn't hard but it can be a bit frustrating for newcomers. This short guide will take you through downloading, installing and setting up Python for the first time.

###Download Python
Downloading Python is the first step in this process, and it's also the easiest. To get started, visit <https://www.python.org/downloads/> and download the featured download appropriate for your operating system. The first thing you will have to decide is which version of Python you would like to run. Unlike most programming languages this is actually a harder decision than you may think, several years ago, a new version of Python was released, this version was Python 3. The release of Python 3 caused quite some controversy as Python 3 was not fully backwards compatible with Python 2, because of this, the development community was split, and there continues to be a divide today between Python 2 and 3 programmers. The latest stable release version of Python 3 is a good place to start as you will most likely have the best compatibility with new programs. However, if you would like to maximize your compatibility with old programs you may want to use Python 2, but there may be some issues when learning to code Python with these lessons.
