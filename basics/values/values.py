#Ints
print(58)	#58
print(7 + 7)	#14

#Floats
print(0.7777 + 0.7777) #1.554
print(0.5 * 2)         #1

#Bools
print(True) #True

#Issues with values and types
print("7" + "7") #77
