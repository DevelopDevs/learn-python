#Values

[Code](./values.py)
There are different kinds of values in Go. The three most basic types of values are strings, ints and bools. It is important that when we perform actions in Go we know what types of values we are working with.

###Basic Value Types
* String - `string`, stores text.
* Integer - `int`, stores a whole number.
* Float - `float`, stores a decimal number.
* Boolean - `bool`, stores True or False.

###Understand what data types you are using
Different data types have different uses, and some just don't work for certain tasks. You can't use ints to do math with decimals, and likewise if you are using whole numbers you shouldn't use floats.
Knowing what value types you are using when writing in Python is important. If we were to perform the following action of adding together two strings with the value of 7, `print("7"+"7")` the action would print `77` not `14`.
This occurs because when you add strings together, they are concatenated, in order to print `14` you would need to write `fmt.Println(7+7)` which adds two integers.

###Next

* [Home](../../README.md)
* [Basics Overview](../basics.md)
* [Next: Variables and Constants](../vars-consts/vars-consts.md)
