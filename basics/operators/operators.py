#Arithmetic Operations
#Addition
print("5 + 2 =", 5+2) #7

#Subtraction
print("5 - 2 =", 5-2) #3

#Multiplication
print("5 * 2 =", 5*2) #10

#Division
print("10 / 2 =", 10/2) #5
print("9 / 2 =", 9/2)   #4

#Modulus
print("10 percent 2 =", 10%2) #0
print("9 percent 2 =", 9%2)   #1

#Logic Operations
#OR
print("True or False =", True or False) #True

#AND
print("True and False =", True and False) #True

#NOT
print("not True =", not True)   #False
print("not False =", not False) #True

#Comparison Operations will be used in the next section, if-else.
#is equal to
print("10 == 2 =", 10 == 2) #False

#is not equal to
print("10 != 2 =", 10 != 2) #True

#is greater than or equal to
print("10 >= 2 =", 10 >= 2) #True

#is greater than
print("10 > 2 =", 10 > 2) #True

#is less than or equal to
print("10 <= 2 =", 10 <= 2) #False

#is less than
print("10 < 2 =", 10 < 2) #False
