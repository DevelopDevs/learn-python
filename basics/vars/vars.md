#Variables

[Code](vars.py)

Here we learn about the two ways in which we store values.
Values can be stored as a variable or a constant. Variables are proactively managed in Python, and their value can change through the course of a program. You can also delete variables with the `del` statement. Unlike most programming languages, Python does not have constants.

###Interacting with variables

* Setting variables
	The primary way that we interact with variables is by setting their values. The first time we set the value of a variable, the variable is created and each subsequent change updates the variable. The datatype of the variable can change when the variable is updated. The syntax for setting and updating variables is, `VARIABLENAME = VALUE`.
	For example: `userName = "Hugo"`, allows us to perform the action `print(userName)` and `Hugo` will be printed.

* Variables reliant on other variables
	When creating variables you will often find yourself relying on other variables to form the value for the new variable. It is important to note that when you make set variables with reliance on another variable, the value is set and then the relation is cut. If you have variable x, and it is reliant on variable y, when you update variable y you do not change the value of variable x.
	For example: The following code explains this concept,

	```py
	r = 255
	g = 10
	b = g + 10
	print(r, g, b)	#255 10 20
	#When we update g, b stays the same because it is assigned the value g + 10 at onee moment not continuosly
	g = 100
	print(r, g, b)	#255 100 20
	```

###Next

* [Home](../../README.md)
* [Basics Overview](../basics.md)
* [Next: Operators](../operators/operators.md)
