#Basic usage
userName = "Hugo"
print(userName)	#Hugo
userName = "Updated name"
print(userName)	#Updated name

#Variables can change data type
myVar = 7
print(myVar)	#7
myVar = "Now a string"
print(myVar)	#Now a string

#Variables reliant on other variables
r = 255
g = 10
b = g + 10
print(r, g, b)	#255 10 20
#When we update g, b stays the same because it is assigned the value g + 10 at onee moment not continuosly
g = 100
print(r, g, b)	#255 100 20
