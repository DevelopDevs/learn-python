#Basics Overview
This section will introduce students to the basics of go. You will be able to make single package systems that can perform basic calculations and logical tasks.

##Lessons
1. [Printing](./printing/printing.md)
1. [Values](./values/values.md)
1. [Variables](vars/vars.md)
1. [Operators](./operators/operators.md)
1. [If Else Statements](./if-else/if-else.md)
1. [For Loops](for/for.md)
1. [Challenge 1: Count to 20 with Even and Odd](../challenges/basics/20-even-odd/20-even-odd.md)

[Home](../README.md)
