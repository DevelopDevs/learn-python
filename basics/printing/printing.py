#Printing strings
print("Hello world!")
#Concatenating strings
print("Hello " + "world!")

#Printing ints
print(7 + 7)
#Printing integers by parsing the 7 string as an int
print(9 + int("9"))
