#Printing

[Code](./printing.py)
As this is the first lesson there will be some basics to go over. Printing is one of the most basic operations in any functional language. Python allows you to easily print values.

###Basic Types of Printing
The basic way to print statements is with the `print()` function. We can print multiple things in the same statement by separating them with commas. We use backslash n inside of a printed string to print a new line. We will learn more about data types and values in the next lesson, but for now, just understand that when print data, data can come in different data types, and the data type must be the same for all piece of data for print to work.

1. Printing Strings - We start by printing the first thing that everyone writes when learning to program, we print `Hello World!`. To do this we write in our program, `print("Hello world!")`. The words need to go inside speech marks to let python know that we are printing a string which is a piece of text. We can also print multiple strings together by concatenating (fancy word, but it just means that we take two strings and add them together to make one long string) them together. We can write the same statement as before but using string concatenation, `print("Hello " + "world!")`, notice how a space is included after the word hello, this is because when concatenating strings you have to manually include the spaces between the words.

1. Printing Ints - The next thing we can do is print whole numbers, or integers. To do this we add to our code a similar statement as before `print(7 + 7)`, this will print 14. If we were to add speech marks around the numbers as follows, `print("7" + "7")`, 77 would be printed because each 7 is treated as a string character, therefore they are concatenated together.

1. Parsing different datatypes - Python doesn't allow you to use different datatypes (strings, ints, etc.) in the same print statement. You can use parsing functions that take data and parse it so it becomes a different datatype. You can use `str()` to parse an int to a string.
	For example: `str(7)` is the same as `"7"`

###Next

* [Home](../../README.md)
* [Basics Overview](../basics.md)
* [Next: Values](../values/values.md)
